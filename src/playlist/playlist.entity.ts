import { Song } from 'src/songs/song.entity';
import { User } from 'src/users/user.entity';
import {
  BaseEntity,
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Playlist extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  title: string;

  @ManyToOne(() => User, (user: User) => user.playlists)
  user: User;
  
  @ManyToMany(() => Song)
  @JoinTable()
  public songs: Song[];
}
