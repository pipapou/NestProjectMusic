import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreatePlaylistDto } from './dto/create-playlist.dto';
import { Playlist } from './playlist.entity';
import { PlaylistRepository } from './playlist.repository';

@Injectable()
export class PlaylistService {
  constructor(
    @InjectRepository(PlaylistRepository)
    private playlistRepository: PlaylistRepository,
  ) {}

  async getAllPlaylist(): Promise<Playlist[]> {
    const found: Playlist[] = await this.playlistRepository.find();
    if (!found) {
      throw new NotFoundException(`Playlist not found`);
    }
    return found;
  }
  async getPlaylistById(id: number): Promise<Playlist> {
    const found = await this.playlistRepository.findOne(id, {
      relations: ['artist', 'songs'],
    });
    if (!found) {
      throw new NotFoundException(`Playlist with ID ${id} not found`);
    }

    return found;
  }
  async createPlaylist(createPlaylistDto: CreatePlaylistDto) {
    const insert = await this.playlistRepository.createPlaylist(createPlaylistDto);
    if (!insert) {
      throw new Error('Playlist not inserted');
    }

    return insert;
  }

  async updateOnePlaylist(id: number, createPlaylistDto: CreatePlaylistDto) {
    const playlist = await this.getPlaylistById(id);
    if (!playlist) {
      throw new NotFoundException(`Playlist with ID ${id} not found`);
    }
    playlist.title = createPlaylistDto.title;

    playlist.save();

    return playlist;
  }

  async deletePlaylistById(id: number): Promise<void> {
    const deleted = await this.playlistRepository.delete(id);
    if (deleted.affected === 0) {
      throw new NotFoundException(`Playlist with ID "${id}" not found`);
    }
  }
}
