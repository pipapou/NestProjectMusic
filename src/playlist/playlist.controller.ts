import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
} from '@nestjs/common';
import { CreatePlaylistDto } from './dto/create-playlist.dto';
import { Playlist } from './playlist.entity';
import { PlaylistService } from './playlist.service';

@Controller('playlist')
export class PlaylistController {
  constructor(private playlist: PlaylistService) {}

  @Get('/all')
  async getAllPlaylist(): Promise<Playlist[]> {
    return this.playlist.getAllPlaylist();
  }

  @Get('/:id')
  async getPlaylistById(@Param('id', ParseIntPipe) id: number): Promise<Playlist> {
    return this.playlist.getPlaylistById(id);
  }

  @Post('/createPlaylist')
  createPlaylist(@Body() createPlaylistDto: CreatePlaylistDto) {
    return this.playlist.createPlaylist(createPlaylistDto);
  }

  @Delete('/:id')
  async deletePlaylistById(@Param('id', ParseIntPipe) id: number): Promise<void> {
    return this.playlist.deletePlaylistById(id);
  }

  @Patch('/updatePlaylist/:id')
  updateOneTask(
    @Param('id', ParseIntPipe) id: number,
    @Body() createPlaylistDto: CreatePlaylistDto,
  ) {
    return this.playlist.updateOnePlaylist(id, createPlaylistDto);
  }
}
