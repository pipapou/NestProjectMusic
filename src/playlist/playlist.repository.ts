import { EntityRepository, Repository } from 'typeorm';
import { Playlist } from './playlist.entity';
import { CreatePlaylistDto } from './dto/create-playlist.dto';

@EntityRepository(Playlist)
export class PlaylistRepository extends Repository<Playlist> {
  async createPlaylist(createPlaylistDto: CreatePlaylistDto): Promise<Playlist> {
    const { title, userId } = createPlaylistDto;

    const playlist = new Playlist();
    playlist.title = title;
    playlist.user = userId;

    await playlist.save();
    return playlist;
  }
}
