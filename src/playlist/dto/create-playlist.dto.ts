import { IsNotEmpty } from 'class-validator';
import { Song } from 'src/songs/song.entity';
import { User } from 'src/users/user.entity';

export class CreatePlaylistDto {
  @IsNotEmpty()
  title: string;

  @IsNotEmpty()
  userId: User;
}
