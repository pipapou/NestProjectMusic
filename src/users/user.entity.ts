import { Album } from 'src/albums/album.entity';
import { Playlist } from 'src/playlist/playlist.entity';
import {
  BaseEntity,
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ unique: true })
  mail: string;
  @Column()
  password: string;

  @OneToMany(() => Playlist, (playlist: Playlist) => playlist.user, {
    cascade: true,
  })
  playlists: Playlist[];
}
