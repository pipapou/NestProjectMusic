import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { AlbumsModule } from './albums/albums.module';
import { ArtistsModule } from './artists/artists.module';
import { SongsModule } from './songs/songs.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeormConfig } from './config/typeormconfig';
import { PlaylistModule } from './playlist/playlist.module';

@Module({
  imports: [ UsersModule, AlbumsModule, ArtistsModule, SongsModule, PlaylistModule, TypeOrmModule.forRoot(typeormConfig)],
  controllers: [],
  providers: [],
})
export class AppModule {}
