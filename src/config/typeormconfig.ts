import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export const typeormConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: process.env.DATABASE_HOST || 'localhost',
  // port: +process.env.DATABASE_PORT || 5433, // port pcfixe
  port: +process.env.DATABASE_PORT || 5432, //port pcportable
  username: process.env.DATABASE_USER || 'postgres',
  password: process.env.DATABASE_PASSWORD || 'root',
  database: process.env.DATABASE_DATABASE || 'albumcollection',
  autoLoadEntities: true,
  synchronize: true,
};
