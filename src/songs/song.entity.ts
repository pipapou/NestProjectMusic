import { Album } from 'src/albums/album.entity';
import { Playlist } from 'src/playlist/playlist.entity';
import {
  BaseEntity,
  Column,
  Entity,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Song extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  title: string;
  @Column()
  duration: string;

  @ManyToOne(() => Album, (album: Album) => album.songs, {
    onDelete: 'CASCADE',
  })
  public album: Album;

  @ManyToMany(() => Playlist, (playlist: Playlist) => playlist.songs, {
    onDelete: 'CASCADE',
  })
  public playlist: Playlist;
}
